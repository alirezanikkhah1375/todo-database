<?php require 'connection.php';

// for adding user . . .

if (isset($_POST["newUserSubmit"])) {
    $newFirstname = $_POST["firstname"];
    $newLastname = $_POST["lastname"];
    if (!empty($newFirstname) && !empty($newLastname)) {
        $sql = "INSERT INTO persons(firstname,lastname)
        VALUES ('$newFirstname', '$newLastname');";
        $insert = mysqli_query($conn, $sql);
        if ($insert) {
            header('location: index.php');
        } else {
            echo "there is a error in query part!";
        }
    } else {
        echo "please enter firstname and lastname <a href='index.php'>TRY AGAIN.</a>";
    }
};

// for adding task . . .

if (isset($_POST["newTaskSubmit"])) {
    $newTask = $_POST["task"];
    if (!empty($newTask)) {
        $sql = "INSERT INTO tasks(task)
        VALUES ('$newTask');";
        $insert = mysqli_query($conn, $sql);
        if ($insert) {
            header('location: index.php');
        } else {
            echo "there is a error in query part!";
        }
    } else {
        echo "please enter the task <a href='index.php'>TRY AGAIN.</a>";
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>to do list</title>
</head>

<body>

    <h1>Tasks table</h1>
    <table>
        <tr>
            <th>😃</th>
            <th>Task !!</th>
            <th>Table id ?!</th>
            <th>Who ?!</th>
            <th>Modification / Edit</th>
        </tr>



        <!-- displaying elements in database . . . -->



        <?php

        $tasks = "SELECT * FROM tasks";
        $tasksResult = mysqli_query($conn, $tasks);

        if (mysqli_num_rows($tasksResult) > 0) {

            $id = 1;
            // output data of each row
            while ($row = mysqli_fetch_array($tasksResult)) { ?>

                <tr>
                    <td><?php echo $id ?></td>
                    <td><?php echo $row["task"] ?></td>
                    <td><?php echo $row["taskID"] ?></td>
                    <td>person</td>
                    <td><a href="<?php echo "delete.php?taskID=" . $row["taskID"] ?>" class="button-task">DELETE</a> <a href="<?php echo "edit.php?id=" . $row["taskID"] ?>" class="button-edit">EDIT</a></td>
                </tr>
        <?php
                $id += 1;
            }
        } else {
            echo "0 results";
        }
        ?>

    </table>



    <!-- add user part . . . -->


    <div>
        <form method="post">
            <button name="addTask" class="add-button"> Add Task</button>
        </form>
    </div>

    <?php if (isset($_POST['addTask'])) { ?>

        <form method="POST">
            <input type="text" name="task" class="input" placeholder="New task">
            <button type="submit" name="newTaskSubmit" class="button-send">Add</button>
        </form>

    <?php } ?>

    <h1>Persons table</h1>

    <table>
        <tr>
            <th>😃</th>
            <th>firstname</th>
            <th>lastname</th>
            <th>Modification</th>
        </tr>



        <!--    user part     -->



        <?php

        $persons = "SELECT * FROM persons";
        $personsResult = mysqli_query($conn, $persons);

        if (mysqli_num_rows($personsResult) > 0) {

            $id = 1;
            // output data of each row
            while ($row = mysqli_fetch_array($personsResult)) { ?>

                <tr>
                    <td><?php echo $id ?></td>
                    <td><?php echo $row["firstname"] ?></td>
                    <td><?php echo $row["lastname"] ?></td>
                    <td><a href="<?php echo "delete.php?personID=" . $row["personID"] ?>" class="button-task">DELETE</a></td>
                </tr>
        <?php
                $id += 1;
            }
        } else {
            echo "0 results";
        }
        ?>

    </table>



    <!-- add user -->



    <div>
        <form method="post">
            <button name="addUser" class="add-button">Add User</button>
        </form>
    </div>

    <?php if (isset($_POST['addUser'])) { ?>

        <div style="margin-bottom: 100px;">
            <form method="POST">
                <input type="text" name="firstname" placeholder="Firstname" class="input">
                <input type="text" name="lastname" placeholder="Lastname" class="input">
                <button type="submit" name="newUserSubmit" class="button-send">Add</button>
            </form>
        </div>

    <?php } ?>



</body>

</html>