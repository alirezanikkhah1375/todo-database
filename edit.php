<?php

require 'connection.php';
$id = $_REQUEST['id'];  // getting id from url . . .
$request = mysqli_query($conn, "SELECT * from tasks WHERE taskID ='$id'"); // choosing yhe text that we want .
$fetch = mysqli_fetch_array($request);  // getting columns of table  . . .
$text = $fetch['task']; // My txty . . .
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>edit form page</title>
</head>

<body>

    <form method="post">
        <input type="text" value="<?php echo $text ?>" placeholder="Please enter the task !" name="task">
        <input type="hidden" name="id" value="<?php echo $id ?>">
        <input type="submit" value="Edit" name="Submit">
    </form>

    <?php

    if (isset($_POST['Submit'])) {
        $editedValue = $_POST['task']; // added info from form 
        if (!empty($editedValue)) {
            $query = "UPDATE tasks SET task = '$editedValue' where taskID = $id ";
            $request = mysqli_query($conn, $query);
            if ($request) {
                header("Location: index.php");
            } else {
                echo '<p class="error">one big error happened about request</p>';
            }
        } else {
            echo " please enter the task ! ";
        }
    }

    ?>

</body>

</html>